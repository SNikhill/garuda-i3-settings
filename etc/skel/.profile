ibus-daemon -d -x
export EDITOR=/usr/bin/micro
export BROWSER=librewolf
export TERM=alacritty
export MAIL=thunderbird
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
